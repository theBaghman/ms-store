package com.baghman.Store.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ConfigDto {
    @JsonProperty("spring.datasource.hikari.connection-timeout")
    int connectionTimeout;
    @JsonProperty("spring.datasource.hikari.idle-timeout")
    int idleTimeout;
    @JsonProperty("spring.datasource.hikari.leak-detection-threshold")
    int leakDetectionThreshold;
    @JsonProperty("spring.datasource.hikari.max-lifetime")
    int maxLifetime;
    @JsonProperty("spring.datasource.hikari.maximum-pool-size")
    int maximumPoolSize;
    @JsonProperty("spring.datasource.hikari.minimum-idle")
    int minimumIdle;
    @JsonProperty("logging.level.com.zaxxer.hikari")
    String hikari;
    @JsonProperty("logging.level.com.zaxxer.hikari.HikariConfig")
    String hikariConfig;
}
