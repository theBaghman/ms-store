package com.baghman.Store.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StoreDto {
    String storeName;
//    @Builder.Default
//    Set<CustomerDto> frequentCustomers = new HashSet<>();
}
