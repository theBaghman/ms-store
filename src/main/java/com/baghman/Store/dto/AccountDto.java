package com.baghman.Store.dto;

import com.baghman.Store.model.Currency;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountDto {

    String accountName;
    Currency currency;

    @Builder.Default
    BigDecimal balance = BigDecimal.ZERO;

}
