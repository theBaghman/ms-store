package com.baghman.Store.dto;

import com.baghman.Store.dto.deserializer.CustomDateDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CustomerDto {
    String customerName;
    String customerNumber;
    @JsonDeserialize(using = CustomDateDeserializer.class)
    @Builder.Default
    LocalDate customerCreatedAt = LocalDate.now();
    @Builder.Default
    List<AccountDto> customerAccounts = new ArrayList<>();
    @Builder.Default
    Set<StoreDto> visitedStores = new HashSet<>();
}

