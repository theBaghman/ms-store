package com.baghman.Store.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "CONFIG")
public class Config {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "conf_id", nullable = false)
    Long id;

    @Column(name = "connection_timeout")
    int connectionTimeout;
    @Column(name = "idle_timeout")
    int idleTimeout;
    @Column(name = "leak_detection_threshold")
    int leakDetectionThreshold;
    @Column(name = "max_lifetime")
    int maxLifetime;
    @Column(name = "maximum_pool_size")
    int maximumPoolSize;
    @Column(name = "minimum_idle")
    int minimumIdle;
    @Column(name = "hikari")
    String hikari;
    @Column(name = "hikari_config")
    String hikariConfig;
}
