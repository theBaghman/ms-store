package com.baghman.Store.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "ACCOUNT")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "acc_id", nullable = false)
    Long id;

    @Column(name = "acc_name", nullable = false)
    String accountName;
    @Column(name = "acc_currency", nullable = false)
    Currency currency;
    @Column(name = "acc_balance", nullable = false)
    @Builder.Default
    BigDecimal balance = BigDecimal.ZERO;

    @ManyToOne
    @JoinColumn(name = "customer_cus_id", referencedColumnName = "cus_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    Customer customer;

}
