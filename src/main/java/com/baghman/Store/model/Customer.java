package com.baghman.Store.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "CUSTOMER")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cus_id", nullable = false)
    Long id;
    @Column(name = "cus_name", nullable = false)
    String customerName;
    @Column(name = "cus_number")
    String customerNumber;
    @Column(name = "cus_created_at", nullable = false)
    @Builder.Default
    LocalDate customerCreatedAt = LocalDate.now();
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "customer",
            fetch = FetchType.LAZY)
    List<Account> customerAccounts;

    @ManyToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    @JoinTable(
            name = "customer_store",
            joinColumns = @JoinColumn(name = "cus_id"),
            inverseJoinColumns = @JoinColumn(name = "sto_id"))
    Set<Store> visitedStores;


}

