package com.baghman.Store.controller;

import com.baghman.Store.dto.ConfigDto;
import com.baghman.Store.dto.CustomerDto;
import com.baghman.Store.service.StoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/store")
@RequiredArgsConstructor
public class StoreController {
    private final StoreService service;

    @PostMapping("/config")
    public ResponseEntity<?> saveConfig(@RequestBody ConfigDto configDto) {
        return Optional.of(service.saveConfig(configDto))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/configs")
    public ResponseEntity<?> saveConfig(@RequestBody List<ConfigDto> configDtos) {
        return Optional.of(service.saveConfigs(configDtos))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/customer")
    public CustomerDto saveCustomer(@RequestBody CustomerDto customerDto) {
        return service.saveCustomer(customerDto);
    }

    @GetMapping("/{customerName}")
    public ResponseEntity<?> findFirstByCustomerName(@PathVariable String customerName) {
        return service.findFirstByCustomerName(customerName)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
}
