package com.baghman.Store.repository;

import com.baghman.Store.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer,Long> {
    Optional<Customer> findFirstByCustomerNameOrderByCustomerCreatedAtDesc(String customerName);
    Optional<Customer> findByCustomerNameIgnoreCase(String customerName);

}
