package com.baghman.Store.service.impl;

import com.baghman.Store.dto.ConfigDto;
import com.baghman.Store.dto.CustomerDto;
import com.baghman.Store.mapper.ConfigMapper;
import com.baghman.Store.mapper.CustomerMapper;
import com.baghman.Store.model.Customer;
import com.baghman.Store.repository.ConfigRepository;
import com.baghman.Store.repository.CustomerRepository;
import com.baghman.Store.service.StoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class StoreServiceImpl implements StoreService {

    private final CustomerRepository customerRepository;
    private final ConfigRepository configRepository;

    @Override
    public ConfigDto saveConfig(ConfigDto configDto) {
        return configMapper.ConfigToConfigDto(configRepository.save(configMapper.ConfigDtoToConfig(configDto)));
    }

    @Override
    public List<ConfigDto> saveConfigs(List<ConfigDto> configDtos) {
        return configMapper.ConfigsToConfigDtos(configRepository.saveAll(configMapper.ConfigDtosToConfigs(configDtos)));
    }

    private final CustomerMapper mapper;
    private final ConfigMapper configMapper;

    @Override
    public CustomerDto saveCustomer(CustomerDto customerDto) {
        Customer customer = mapper.customerDtoToCustomer(customerDto);
        customer.getCustomerAccounts()
                .forEach(account -> account.setCustomer(customer));
        customer.getVisitedStores()
                .forEach(store -> store.setFrequentCustomers(Set.of(customer)));
        return mapper.customerToCustomerDto(customerRepository.save(customer));

    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CustomerDto> findFirstByCustomerName(String customerName) {
        return customerRepository.findFirstByCustomerNameOrderByCustomerCreatedAtDesc(customerName)
                .map(mapper::customerToCustomerDto);
    }
}
