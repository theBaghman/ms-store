package com.baghman.Store.service;

import com.baghman.Store.dto.ConfigDto;
import com.baghman.Store.dto.CustomerDto;

import java.util.List;
import java.util.Optional;

public interface StoreService {
    CustomerDto saveCustomer(CustomerDto customerDto);

    Optional<CustomerDto> findFirstByCustomerName(String customerName);

    ConfigDto saveConfig(ConfigDto configDto);

    List<ConfigDto> saveConfigs(List<ConfigDto> configDtos);
}
