package com.baghman.Store.mapper;

import com.baghman.Store.dto.ConfigDto;
import com.baghman.Store.model.Config;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ConfigMapper {
    Config ConfigDtoToConfig(ConfigDto ConfigDto);

    ConfigDto ConfigToConfigDto(Config Config);

    List<Config> ConfigDtosToConfigs(List<ConfigDto> ConfigDtos);

    List<ConfigDto> ConfigsToConfigDtos(List<Config> Configs);
}
