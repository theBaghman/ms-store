package com.baghman.Store.mapper;

import com.baghman.Store.dto.StoreDto;
import com.baghman.Store.model.Store;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.Set;
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)

public interface StoreMapper {
    Store StoreDtoToStore(StoreDto StoreDto);

    StoreDto StoreToStoreDto(Store Store);

    Set<Store> StoreDtosToStores(Set<StoreDto> StoreDtos);

    Set<StoreDto> StoresToStoreDtos(Set<Store> Stores);
}
