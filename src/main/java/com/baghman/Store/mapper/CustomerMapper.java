package com.baghman.Store.mapper;

import com.baghman.Store.dto.CustomerDto;
import com.baghman.Store.model.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CustomerMapper {
    Customer customerDtoToCustomer(CustomerDto customerDto);

    CustomerDto customerToCustomerDto(Customer customer);

    List<Customer> customerDtosToCustomers(List<CustomerDto> customerDtos);

    List<CustomerDto> customersToCustomerDtos(List<Customer> customers);
}
