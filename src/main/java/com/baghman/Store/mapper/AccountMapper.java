package com.baghman.Store.mapper;

import com.baghman.Store.dto.AccountDto;
import com.baghman.Store.model.Account;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AccountMapper {
    Account AccountDtoToAccount(AccountDto AccountDto);

    AccountDto AccountToAccountDto(Account Account);

    List<Account> AccountDtosToAccounts(List<AccountDto> AccountDtos);

    List<AccountDto> AccountsToAccountDtos(List<Account> Accounts);
}
