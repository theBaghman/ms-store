package com.baghman.Store;

import com.baghman.Store.dto.StoreDto;
import com.baghman.Store.mapper.StoreMapper;
import com.baghman.Store.model.Customer;
import com.baghman.Store.model.Store;
import com.baghman.Store.repository.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.HashSet;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class StoreControllerIntegrationTest {
    @Autowired
    private CustomerRepository repository;
    @Autowired
    private MockMvc mockMvc;

    private StoreDto storeDto20;
    private StoreDto storeDto28;
    @Autowired
    private StoreMapper storeMapper;


    @BeforeEach
    void setUp() {
        Customer Customer = new Customer();
        Customer.setCustomerName("Mahammad");
        Store store20 = Store.builder().storeName("20YanvarBravo").build();
        Store store28 = Store.builder().storeName("28MayBravo").build();
        Customer.setVisitedStores(new HashSet<>(Arrays.asList(store20,store28)));

        storeDto20 = storeMapper.StoreToStoreDto(store20);
        storeDto28 = storeMapper.StoreToStoreDto(store28);

        repository.save(Customer);
    }

//    @Test
//    void givenTheCustomerExists_WhenOsivIsEnabled_ThenLazyInitWorksEverywhere() throws Exception {
//        mockMvc.perform(get("/customer/Mahammad"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.customerName").value("Mahammad"))
//                .andExpect(jsonPath("$.visitedStores").value(Matcher));
//    }
}
